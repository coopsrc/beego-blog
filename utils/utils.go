package utils

import (
	"strconv"
	"time"
	"fmt"
	"crypto/sha1"
	"crypto/md5"
	"encoding/base64"
)

func GenPassword(password string) string {
	salt := strconv.FormatInt(time.Now().UnixNano() % 9000 + 1000, 10)
	return Base64Encode(Sha1(Md5(password) + salt) + salt)
}

func CheckPassword(pwd, saved string) bool {
	saved = Base64Decode(saved)
	if len(saved) < 4 {
		return false
	}
	salt := saved[len(saved) - 4:]
	return Sha1(Md5(pwd) + salt) + salt == saved
}

func Sha1(s string) string {
	return fmt.Sprintf("%x", sha1.Sum([]byte(s)))
}

func Md5(s string) string {
	return fmt.Sprintf("%x", md5.Sum([]byte(s)))
}

func Base64Encode(s string) string {
	return base64.StdEncoding.EncodeToString([]byte(s))
}

func Base64Decode(s string) string {
	res, _ := base64.StdEncoding.DecodeString(s)
	return string(res)
}
