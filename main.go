package main

import (
	_ "dewdrop-blog/routers"
	_"dewdrop-blog/database"
	"github.com/astaxie/beego"
	"encoding/gob"
	"dewdrop-blog/models"
)

func init() {
	gob.Register(models.User{})
	gob.Register(models.Blog{})
	gob.Register(models.Category{})
	gob.Register(models.Tag{})
	gob.Register(models.Comment{})
	gob.Register(models.View{})
	gob.Register(models.Host{})
}

func main() {
	beego.Run()
}

