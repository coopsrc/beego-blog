package controllers

import (
	"fmt"
	"strconv"
	"github.com/astaxie/beego/validation"
	"dewdrop-blog/models"
	"strings"
)

type TagController struct {
	BaseController
	ret RET
}

func (controller *TagController)Post() {
	if !controller.IsLogin() {
		controller.ret.Status = false
		controller.ret.Content = "尚未登录"
		controller.Data["json"] = controller.ret
		controller.ServeJSON()
		return
	}

	tag := strings.Trim(controller.GetString("tag"), " ")

	if len(tag) == 0 {
		controller.ret.Status = true
		controller.ret.Content = "未添加任何标签"
		controller.Data["json"] = controller.ret
		controller.ServeJSON()
		return
	}

	valid := validation.Validation{}

	valid.Required(tag, "Tag")

	data := strings.Split(tag, ",")

	var tags map[string]string
	tags = make(map[string]string)

	for _, t := range data {

		tag := &models.Tag{
			Name:strings.ToLower(t),
			DisplayName:t,
		}
		switch {
		case tag.ExistName():
			tag.GetByName()
			tags[strconv.Itoa(len(tags))] = fmt.Sprintf("%d", tag.Id)
		default:
			id, err := tag.Create()

			if err == nil {
				tags[strconv.Itoa(len(tags))] = fmt.Sprintf("%d", id)
			}
			valid.Error(fmt.Sprintf("%v", err))
		}
	}

	if len(tags) > 0 {
		controller.ret.Status = true
		controller.ret.Content = tags
		controller.Data["json"] = controller.ret
		controller.ServeJSON()
		return
	}

	controller.ret.Status = false
	controller.ret.Content = valid.Errors[0].Key + " : " + valid.Errors[0].Message
	controller.Data["json"] = controller.ret
	controller.ServeJSON()
	return
}

func (controller *TagController)Get() {
	tag := &models.Tag{}
	cate_list := tag.GetAll()

	controller.Data["json"] = cate_list

	controller.ServeJSON()
	return
}

func (controller *TagController)GetById() {
	id, _ := strconv.ParseInt(controller.Ctx.Input.Param(":id"), 10, 64)
	tag := &models.Tag{Id:id}
	tag.Read()

	controller.Data["json"] = tag

	controller.ServeJSON()
	return
}