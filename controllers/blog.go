package controllers

import (
	"fmt"
	"dewdrop-blog/models"
	"github.com/astaxie/beego/validation"
	"strconv"
	"time"
)

type BlogController struct {
	BaseController
	ret RET
}

func (controller *BlogController)Post() {
	if !controller.IsLogin() {
		controller.ret.Status = false
		controller.ret.Content = "尚未登录"
		controller.Data["json"] = controller.ret
		controller.ServeJSON()
		return
	}
	user := controller.GetSession("user").(models.User)

	title := controller.GetString("title")
	content := controller.GetString("content")
	summary := controller.GetString("summary")
	cate_id, _ := strconv.ParseInt(controller.GetString("category"), 10, 64)
	//tags := controller.GetString("tags")

	valid := validation.Validation{}

	valid.Required(title, "Blog Title")
	valid.Required(content, "Blog Content")
	valid.Required(summary, "Blog Summary")
	valid.Required(cate_id, "Category")

	valid.MaxSize(title, 80, "Blog Title")

	switch {
	case valid.HasErrors():
	default:
		category := &models.Category{Id:cate_id}
		category.Read()

		blog := &models.Blog{
			Id:time.Now().UnixNano(),
			Title:title,
			Content:content,
			Summary:summary,
			Author:&user,
			Category:category,
		}

		_, err := blog.Create()

		if err == nil {
			controller.ret.Status = true
			controller.ret.Content = blog.Id
			controller.Data["json"] = controller.ret
			controller.ServeJSON()
			return
		}
		valid.Error(fmt.Sprintf("%v", err))
	}

	controller.ret.Status = false
	controller.ret.Content = valid.Errors[0].Key + " : " + valid.Errors[0].Message
	controller.Data["json"] = controller.ret
	controller.ServeJSON()
	return
}

func (controller *BlogController) Get() {
	blog := &models.Blog{}
	blog_list := blog.GetAll()
	for _, b := range blog_list {
		b.Author.Read()
		b.Category.Read()
		fmt.Printf("Blog Title : %s\n", b.Title)
	}
	controller.Data["json"] = blog_list

	controller.ServeJSON()

	return
}

func (controller *BlogController) GetById() {

	id, _ := strconv.ParseInt(controller.Ctx.Input.Param(":id"), 10, 64)
	blog := &models.Blog{Id:id}
	blog.Read()
	blog.Author.Read()
	blog.Category.Read()
	fmt.Printf("Blog Title : %s\n", blog.Title)
	controller.Data["json"] = blog

	controller.ServeJSON()

	return
}
