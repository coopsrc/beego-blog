package controllers

import (
	"fmt"
	"dewdrop-blog/models"
	"strconv"
)

type IndexController struct {
	BaseController
	ret RET
}

func (controller *IndexController)Get() {
	category := &models.Category{}
	cate_list := category.GetAll()
	controller.Data["cate_list"] = cate_list

	blog := &models.Blog{}
	blog_list := blog.GetAll()
	for _, b := range blog_list {
		b.Author.Read()
		b.Category.Read()
		fmt.Printf("Blog Title : %s\n", b.Title)
	}
	controller.Data["blog_list"] = blog_list

	controller.TplName = "index.html"
}

func (controller *IndexController) GetById() {

	category := &models.Category{}
	cate_list := category.GetAll()
	controller.Data["cate_list"] = cate_list

	id, _ := strconv.ParseInt(controller.Ctx.Input.Param(":id"), 10, 64)
	blog := &models.Blog{Id:id}
	blog.Read()
	blog.Author.Read()
	blog.Category.Read()
	controller.Data["blog"] = blog

	controller.TplName = "blog.html"
}