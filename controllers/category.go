package controllers

import (
	"dewdrop-blog/models"
	"fmt"
	"strconv"
	"github.com/astaxie/beego/validation"
)

type CategoryController struct {
	BaseController
	ret RET
}

func (controller *CategoryController)Post() {
	if !controller.IsLogin() {
		controller.ret.Status = false
		controller.ret.Content = "尚未登录"
		controller.Data["json"] = controller.ret
		controller.ServeJSON()
		return
	}

	name := controller.GetString("name")
	display_name := controller.GetString("display_name")

	valid := validation.Validation{}

	valid.Required(name, "Name")
	valid.Required(display_name, "DisplayName")

	valid.MinSize(name, 2, "Name")
	valid.MaxSize(name, 10, "Name")
	valid.MinSize(display_name, 2, "DisplayName")
	valid.MaxSize(display_name, 10, "DisplayName")

	switch {
	case valid.HasErrors():
	default:
		category := &models.Category{
			Name:name,
			DisplayName:display_name,
		}

		switch {
		case category.ExistName():
			valid.Error("分类已存在")
		default:
			id, err := category.Create()

			if err == nil {
				controller.ret.Status = true
				controller.ret.Content = id
				controller.Data["json"] = controller.ret
				controller.ServeJSON()
				return
			}
			valid.Error(fmt.Sprintf("%v", err))
		}
	}

	controller.ret.Status = false
	controller.ret.Content = valid.Errors[0].Key + " : " + valid.Errors[0].Message
	controller.Data["json"] = controller.ret
	controller.ServeJSON()
	return
}

func (controller *CategoryController)Get() {
	category := &models.Category{}
	cate_list := category.GetAll()

	controller.Data["json"] = cate_list

	controller.ServeJSON()
	return
}

func (controller *CategoryController)GetById() {
	id, _ := strconv.ParseInt(controller.Ctx.Input.Param(":id"), 10, 64)
	category := &models.Category{Id:id}
	category.Read()

	for _, b := range category.BlogList {
		b.Author.Read()
		b.Category = nil
	}

	controller.Data["json"] = category

	controller.ServeJSON()
	return
}

