package controllers

import (
	"github.com/astaxie/beego/validation"
	"time"
	"fmt"
	"dewdrop-blog/models"
	"dewdrop-blog/utils"
)

type UserController struct {
	BaseController
	ret RET
}

func (controller *UserController)SignUp() {
	username := controller.GetString("username")
	nickname := controller.GetString("nickname")
	email := controller.GetString("email")
	password := controller.GetString("password")

	if len(nickname) < 1 {
		nickname = username
	}

	valid := validation.Validation{}

	valid.Email(email, "Email")

	valid.Required(username, "UserName")
	valid.Required(password, "Password")

	valid.MinSize(username, 6, "UserName")
	valid.MaxSize(username, 20, "UserName")
	valid.MaxSize(nickname, 40, "NickName")
	valid.MinSize(password, 8, "Password")
	valid.MaxSize(password, 16, "Password")

	switch {
	case valid.HasErrors():
	default:
		user := &models.User{
			Id:time.Now().Unix(),
			UserName:username,
			NickName:nickname,
			Email:email,
			Password:utils.GenPassword(password),
			Info:"In me the tiger sniffs the rose",
			Followers:0,
			Following:0,
			RegisterTime:time.Now(),
			Private:models.DefaultPrivate,
		}

		switch {
		case user.ExistId():
			valid.Error("数据库错误")
		case user.ExistUserName():
			valid.Error("用户名已存在")
		case user.ExistEmail():
			valid.Error("邮箱已存在")
		default:
			_, err := user.Create()
			if err == nil {
				controller.ret.Status = true
				controller.ret.Content = user.Id
				controller.Data["json"] = controller.ret
				controller.ServeJSON()
				return
			}
			valid.Error(fmt.Sprintf("%v", err))
		}
	}

	controller.ret.Status = false
	controller.ret.Content = valid.Errors[0].Key + " : " + valid.Errors[0].Message
	controller.Data["json"] = controller.ret
	controller.ServeJSON()

	return
}

func (controller *UserController)LogIn() {
	username := controller.GetString("username")
	password := controller.GetString("password")

	valid := validation.Validation{}

	valid.Required(username, "UserName")
	valid.Required(password, "Password")

	valid.MinSize(username, 6, "UserName")
	valid.MaxSize(username, 20, "UserName")
	valid.MinSize(password, 8, "Password")
	valid.MaxSize(password, 16, "Password")

	user := &models.User{UserName:username}
	err := user.GetByUserName()

	user.Display()

	switch  {
	case valid.HasErrors():
	case err != nil:
		valid.Error("用户不存在")
	case utils.CheckPassword(password, user.Password) == false:
		valid.Error("密码错误")
	default:
		controller.DoLogin(*user)
		controller.ret.Status = true
		controller.ret.Content = "登录成功"
		controller.Data["json"] = controller.ret
		controller.ServeJSON()
		return
	}

	controller.ret.Status = false
	controller.ret.Content = valid.Errors[0].Key + valid.Errors[0].Message
	controller.Data["json"] = controller.ret
	controller.ServeJSON()

	return
}