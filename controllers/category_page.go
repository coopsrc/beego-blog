package controllers

import "dewdrop-blog/models"

func (controller *CategoryController)GetByName() {

	cate := &models.Category{}
	cate_list := cate.GetAll()
	controller.Data["cate_list"] = cate_list

	category := &models.Category{Name:controller.Ctx.Input.Param(":name")}
	category.Get()
	category.Read()

	for _, b := range category.BlogList {
		b.Author.Read()
		b.Category.Read()
	}

	controller.Data["blog_list"] = category.BlogList

	controller.TplName = "index.html"
}
