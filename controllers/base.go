package controllers

import (
	"github.com/astaxie/beego"
	"dewdrop-blog/models"
)

type BaseController struct {
	beego.Controller
}

type RET struct {
	Status  bool `json:"success"`
	Content interface{} `json:"content"`
}

func (controller *BaseController)Prepare() {
	if controller.IsLogin() {
		controller.Data["user"] = controller.GetSession("user").(models.User)
	}
}

func (controller *BaseController) DoLogin(user models.User) {
	controller.SetSession("user", user)
	controller.CheckLogin()
}

func (controller *BaseController)DoLogout() {
	controller.DestroySession()
	controller.Redirect("/signin", 302)
}

func (controller *BaseController) CheckLogin() {
	if !controller.IsLogin() {
		controller.Redirect("/signin", 302)
		controller.Abort("302")
	}
}

func (controller *BaseController) IsLogin() bool {
	return controller.GetSession("user") != nil
}