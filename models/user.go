package models

import (
	"time"
	"fmt"
	"github.com/astaxie/beego/orm"
	"github.com/astaxie/beego"
)

type User struct {
	Id           int64        `orm:"pk"`
	UserName     string        `orm:"index"`
	NickName     string        `orm:"null"`
	Email        string        `orm:"null"`
	Password     string

	Info         string        `orm:"null"`

	Followers    int
	Following    int

	RegisterTime time.Time    `orm:"auto_now_add"`

	Private      int
}

func (user *User)Display() {
	fmt.Printf("User Id : %s\n", user.Id)
	fmt.Printf("User UserName : %s\n", user.UserName)
	fmt.Printf("User NickName : %s\n", user.NickName)
	fmt.Printf("User Email : %s\n", user.Email)
	fmt.Printf("User Password : %s\n", user.Password)
	fmt.Printf("User RegisterTime : %s\n", user.RegisterTime)
	fmt.Printf("User Private : %s\n", user.Private)
}

const (
	DefaultPrivate = 1 << 8 - 1
)

func (user User) Create() (id int64, err error) {
	orm := orm.NewOrm()
	if id, err = orm.Insert(&user); err != nil {
		beego.Info(err)
	}
	return
}

func (user *User)Read() (err error) {
	orm := orm.NewOrm()

	if err = orm.Read(user); err != nil {
		beego.Info(err)
	}

	return
}

func (user User) Update() (id int64, err error) {
	orm := orm.NewOrm()
	if id, err = orm.Update(&user); err != nil {
		beego.Info(err)
	}
	return
}

func (user User) Delete() (id int64, err error) {
	user.Private &= ^1

	if id, err = user.Update(); err != nil {
		beego.Info(err)
	}

	return
}

func (user *User)GetByUserName() (err error) {
	orm := orm.NewOrm()

	rs := orm.Raw("SELECT * FROM user WHERE user_name = ?", user.UserName)

	if err = rs.QueryRow(user); err != nil {
		beego.Info(err)
	}

	return
}

func (user User) ExistId() bool {
	o := orm.NewOrm()
	if err := o.Read(&user); err == orm.ErrNoRows {
		return false
	}
	return true
}

func (user User) ExistUserName() bool {
	o := orm.NewOrm()
	return o.QueryTable("user").Filter("UserName", user.UserName).Exist()
}

func (user User) ExistEmail() bool {
	o := orm.NewOrm()
	return o.QueryTable("user").Filter("Email", user.Email).Exist()
}

