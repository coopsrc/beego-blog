package models

import (
	"time"
	"fmt"
	"github.com/astaxie/beego/orm"
	"github.com/astaxie/beego"
)

type Category struct {
	Id          int64        `orm:"pk;auto"`
	Name        string        `orm:"size(10),index"`
	DisplayName string        `orm:"size(10),index"`

	Count       int
	Order       int

	Time        time.Time    `orm:"auto_now_add"`

	BlogList    []*Blog        `orm:"reverse(many)"`

	Defunct     bool
}

func (category *Category) Display() {
	fmt.Print("================ Category ================\n")
	fmt.Printf("Category Id : %s\n", category.Id)
	fmt.Printf("Category Name : %s\n", category.Name)
	fmt.Printf("Category Count : %s\n", category.Count)
	fmt.Printf("Category Order : %s\n", category.Order)
	fmt.Printf("Category Time : %s\n", category.Time)
	fmt.Printf("Category BlogList : %s\n", category.BlogList)
}

func (category *Category)Get() (err error) {
	orm := orm.NewOrm()

	if err = orm.QueryTable("category").Filter("Name", category.Name).One(category); err != nil {
		beego.Info(err)
	}

	return
}

func (category *Category)Read() (err error) {
	orm := orm.NewOrm()

	if err = orm.Read(category); err != nil {
		beego.Info(err)
	}

	_,_ = orm.LoadRelated(category, "BlogList")
	return
}

func (category Category)Create() (id int64, err error) {
	orm := orm.NewOrm()

	if id, err = orm.Insert(&category); err != nil {
		beego.Info(err)
	}

	return
}

func (category Category)Update() (err error) {
	orm := orm.NewOrm()

	if _, err = orm.Update(&category); err != nil {
		beego.Info(err)
	}

	return
}

func (category Category)Delete() (err error) {
	category.Defunct = true
	err = category.Update()
	return
}


func (category *Category)GetByDisplayName() (err error) {
	orm := orm.NewOrm()

	if err = orm.QueryTable("category").Filter("DisplayName", category.DisplayName).One(category); err != nil {
		beego.Info(err)
	}

	return
}

func (category Category)GetAll() (list []Category) {
	orm := orm.NewOrm()
	orm.QueryTable("category").Filter("defunct", 0).All(&list)
	return
}

func (category Category)ExistName() bool {
	orm := orm.NewOrm()
	return orm.QueryTable("category").Filter("Name", category.Name).Exist()
}