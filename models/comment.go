package models

import "time"

type Comment struct {
	Id      int64 		`orm:"pk;auto"`
	Content string 		`orm:"type(text)`
	Time    time.Time 	`orm:"auto_now_add"`

	Blog    *Blog 		`orm:"rel(fk)"`

	Comment  *Comment 	`orm:"rel(fk)"`
	Parent  []*Comment 	`orm:"reverse(many)"`

	Defunct bool
}
