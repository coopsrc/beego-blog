package models

import "time"

type View struct {
	Id   int64 		`orm:"pk;auto"`
	Time time.Time 	`orm:"auto_now_add"`

	User *Blog		`orm:"rel(fk)"`

	Host *Host		`orm:"rel(one)"`
}
