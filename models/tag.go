package models

import (
	"time"
	"github.com/astaxie/beego/orm"
	"github.com/astaxie/beego"
	"fmt"
)

type Tag struct {
	Id          int64        `orm:"pk;auto"`
	Name        string        `orm:"size(10),index"`
	DisplayName string        `orm:"size(10),index"`

	Count       int
	Order       int

	Time        time.Time    `orm:"auto_now_add"`

	BlogList    []*Blog    `orm:"reverse(many)"`

	Defunct     bool
}

func (tag *Tag) Display() {
	fmt.Print("================ Tag ================\n")
	fmt.Printf("Category Id : %s\n", tag.Id)
	fmt.Printf("Category Name : %s\n", tag.Name)
	fmt.Printf("Category Count : %s\n", tag.Count)
	fmt.Printf("Category Order : %s\n", tag.Order)
	fmt.Printf("Category Time : %s\n", tag.Time)
	fmt.Printf("Category BlogList : %s\n", tag.BlogList)
}

func (tag Tag)Get() (*Tag) {
	orm := orm.NewOrm()
	orm.QueryTable("tag").Filter("Name", tag.Name).One(&tag)
	return &tag
}

func (tag Tag)GetOrNew() (*Tag) {
	orm := orm.NewOrm()
	_,_,_ = orm.ReadOrCreate(&tag, "Name")
	return &tag
}

func (tag *Tag)Read() (err error) {
	orm := orm.NewOrm()

	if err = orm.Read(tag); err != nil {
		beego.Info(err)
	}

	return
}

func (tag Tag)Create() (id int64, err error) {
	orm := orm.NewOrm()

	if id, err = orm.Insert(&tag); err != nil {
		beego.Info(err)
	}

	return
}

func (tag Tag)Update() (err error) {
	orm := orm.NewOrm()

	if _, err = orm.Update(&tag); err != nil {
		beego.Info(err)
	}

	return
}

func (tag Tag)Delete() (err error) {
	tag.Defunct = true
	err = tag.Update()
	return
}

func (tag *Tag)GetByName() (err error) {
	orm := orm.NewOrm()

	rs := orm.Raw("SELECT * FROM tag WHERE name = ?", tag.Name)

	if err = rs.QueryRow(tag); err != nil {
		beego.Info(err)
	}

	return
}

func (tag *Tag)GetByDisplayName() (err error) {
	orm := orm.NewOrm()

	rs := orm.Raw("SELECT * FROM tag WHERE display_name = ?", tag.DisplayName)

	if err = rs.QueryRow(tag); err != nil {
		beego.Info(err)
	}

	return
}

func (tag Tag)GetAll() (list []Tag) {
	orm := orm.NewOrm()
	orm.QueryTable("tag").Filter("Defunct", 0).All(&list)
	return
}

func (tag Tag)ExistName() bool {
	orm := orm.NewOrm()
	return orm.QueryTable("tag").Filter("Name", tag.Name).Exist()
}