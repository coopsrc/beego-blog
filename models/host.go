package models

type Host struct {
	Id      int64	`orm:"pk;auto"`

	Name    string	`orm:"index"`
	Dns     string
	Ip      string
	Mac     string

	Os      string
	Browser string

	View    *View	`orm:"reverse(one)"`
}
