package models

import (
	"time"
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/orm"
	"fmt"
)

type Blog struct {
	Id       int64		`orm:"pk"`

	Title    string 	`orm:"size(80)"`
	Summary  string 	`orm:"type(text)`
	Content  string 	`orm:"type(text)`

	Author   *User 		`orm:"rel(fk)"`
	Tag      []*Tag		`orm:"rel(m2m)"`
	Category *Category 	`orm:"rel(fk)"`

	Comments []*Comment	`orm:"reverse(many)"`
	Views    []*View	`orm:"reverse(many)"`

	Time     time.Time	`orm:"auto_now_add;type(datetime)"`

	Defunct  bool
}

func (blog *Blog)Display() {
	fmt.Printf("Blog Id : %s\n", blog.Id)
	fmt.Printf("Blog Title : %s\n", blog.Title)
	fmt.Printf("Blog Content : %s\n", blog.Summary)
	fmt.Printf("Blog Content : %s\n", blog.Content)
	fmt.Printf("Blog Author : %s\n", blog.Author)
	fmt.Printf("Blog Commits : %s\n", blog.Comments)
	fmt.Printf("Blog Views : %s\n", blog.Views)
	fmt.Printf("Blog Tag : %s\n", blog.Tag)
	fmt.Printf("Blog Time : %s\n", blog.Time)
	fmt.Printf("Blog Defunct : %s\n", blog.Defunct)
}

func (blog *Blog) Read() (err error) {
	orm := orm.NewOrm()
	if err = orm.Read(blog); err != nil {
		beego.Info(err)
	}

	_,_ = orm.LoadRelated(blog, "Tag")
	_,_ = orm.LoadRelated(blog, "Comments")
	_,_ = orm.LoadRelated(blog, "Views")

	return
}

func (blog Blog) Create() (id int64, err error) {
	orm := orm.NewOrm()
	if id, err = orm.Insert(&blog); err != nil {
		beego.Info(err)
	}
	return
}

func (blog Blog) Update() (err error) {
	orm := orm.NewOrm()
	if _, err = orm.Update(&blog); err != nil {
		beego.Info(err)
	}
	return
}

func (blog Blog) Delete() (err error) {
	blog.Defunct = true
	err = blog.Update()
	return
}

func (blog Blog) GetAll() (list []Blog) {
	orm := orm.NewOrm()
	orm.QueryTable("blog").Filter("defunct", 0).All(&list)
	return
}

func (blog Blog) Gets() (list []Blog) {
	orm := orm.NewOrm()
	orm.QueryTable("blog").Filter("Author", blog.Author).Filter("defunct", 0).All(&list)
	return
}

func (blog Blog) GetByCategory(category Category) (list []Blog) {
	orm := orm.NewOrm()
	orm.QueryTable("blog").Filter("Category", category).Filter("defunct", 0).All(&list)
	return
}