package database

import (
	"fmt"
	"github.com/astaxie/beego/orm"
	"github.com/astaxie/beego"

	_"github.com/go-sql-driver/mysql"
	"dewdrop-blog/models"
)

func init() {
	fmt.Print("init database dewdrop\n")

	orm.RegisterDriver("mysql", orm.DRMySQL)
	orm.RegisterDataBase("default", "mysql", fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?charset=utf8&loc=%s",
		beego.AppConfig.String("MYSQL::db_user"),
		beego.AppConfig.String("MYSQL::db_pass"),
		beego.AppConfig.String("MYSQL::db_host"),
		beego.AppConfig.String("MYSQL::db_port"),
		beego.AppConfig.String("MYSQL::db_name"),
		`Asia%2FShanghai`,
	))

	orm.RegisterModel(new(models.User))
	orm.RegisterModel(new(models.Blog))
	orm.RegisterModel(new(models.Category))
	orm.RegisterModel(new(models.Tag))
	orm.RegisterModel(new(models.Comment))
	orm.RegisterModel(new(models.View))
	orm.RegisterModel(new(models.Host))

	orm.RunSyncdb("default", false, true)

	fmt.Print("init database finish\n")
}
