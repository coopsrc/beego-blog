package routers

import (
	"dewdrop-blog/controllers"
	"github.com/astaxie/beego"
)

func init() {

	beego.Router("/api/signup", &controllers.UserController{}, `post:SignUp`)
	beego.Router("/api/login", &controllers.UserController{}, `post:LogIn`)
	beego.Router("/api/category", &controllers.CategoryController{})
	beego.Router("/api/tag", &controllers.TagController{})
	beego.Router("/api/blog", &controllers.BlogController{})

	beego.Router("/", &controllers.IndexController{})
	beego.Router("/blog/content/:id([0-9]+)", &controllers.IndexController{}, `get:GetById`)
	beego.Router("/blog/category/:name(^[a-zA-Z]+)", &controllers.CategoryController{}, `get:GetByName`)
}
